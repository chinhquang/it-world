//
//  RegistrationViewController.swift
//  ITWorld_Clone
//
//  Created by chinh.tq on 7/23/19.
//  Copyright © 2019 chinh.tq. All rights reserved.
//

import UIKit
import Eureka
class RegistrationViewController: FormViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Registration Form"
        form +++ Section("Personal Information")
            <<< TextRow(){ row in
                row.tag = "fullname"
                row.title = "Fullname"
                row.placeholder = "Enter your full name"
            }
            <<< PhoneRow(){
                $0.tag = "phone"
                $0.title = "Phone Number"
                $0.placeholder = "And numbers here"
            }
            <<< DateRow(){
                $0.tag = "date"
                $0.title = "Date of birth"
                $0.value = Date(timeIntervalSinceReferenceDate: 0)
            }
            <<< TextRow(){ row in
                row.title = "Address"
                row.placeholder = "Enter your address"
            }
            +++ Section("Account information")
            <<< TextRow(){
                row in
                row.title = "Username"
                row.placeholder = "Enter your username"
                
            }
            <<< PasswordRow(){ row in
                row.title = "Password"
                row.placeholder = "Enter your password"
            }
            <<< EmailRow(){ row in
                row.title = "Email"
                row.placeholder = "Enter your email"
                
            }
        
            +++ ButtonRow("Submit") { (row: ButtonRow) in
                row.title = row.tag
            }.onCellSelection({ [weak self] (cell, row) in
                 self?.navigationController?.pushViewController(CategoryViewController(), animated: true)
                
                })
        
    }
}
