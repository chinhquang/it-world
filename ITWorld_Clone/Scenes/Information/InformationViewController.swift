//
//  InformationViewController.swift
//  ITWorld_Clone
//
//  Created by chinh.tq on 7/22/19.
//  Copyright © 2019 chinh.tq. All rights reserved.
//

import UIKit

class InformationViewController: UIViewController {

    @IBAction func checkBoxTap(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBOutlet weak var checkBox: UIButton!{
        didSet{
            self.checkBox.setBackgroundImage(UIImage(named: "unchecked"), for: .normal)
            self.checkBox.setBackgroundImage(UIImage(named: "checked"), for: .selected)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLanguageButton()
       
        // Do any additional setup after loading the view.
        
    }
    @IBAction func NextButtonAction(_ sender: UIButton) {
        navigationController?.pushViewController(MenuViewController(), animated: true)
    }
    @IBAction func exitNow(_ sender: Any) {
        //UIApplication.shared.perform(#selector(susp))
        let alert = UIAlertController(title: "QUIT APP", message: "Are you sure to exit this app", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {
            [weak self]action in
            UIControl().sendAction(#selector(NSXPCConnection.suspend),
                                   to: UIApplication.shared, for: nil)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barStyle = .default
        self.navigationController?.navigationBar.backgroundImage(for: .default)
        self.navigationController?.navigationBar.backgroundColor = .none
        self.navigationController?.navigationBar.setBackgroundImage(.none, for: .default)
        self.navigationController?.setupColor(color: #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1))
        self.navigationItem.title = "Information"
    }
    // MARK: - Set up language-changing Button
    func setupLanguageButton() {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "japan"), for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
    }
    
}

