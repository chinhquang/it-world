//
//  LoginViewController.swift
//  ITWorld_Clone
//
//  Created by chinh.tq on 7/29/19.
//  Copyright © 2019 chinh.tq. All rights reserved.
//

import UIKit
import GoogleSignIn
import FirebaseAuth
import Firebase
class LoginViewController: UIViewController {
    var isSignIn : Bool = false
    var activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    var button : GIDSignInButton = {
        let btn = GIDSignInButton(frame: CGRect(x: 0, y: 0, width: 150, height: 50))
        return btn
    }()
    @IBAction func showGooglePrebuiltUI(_ sender: Any) {
        //GIDSignIn.sharedInstance()?.delegate = self
        
        
        GIDSignIn.sharedInstance()?.signIn()
    }
    @IBOutlet weak var GGBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    func startIndicator()
    {
        //creating view to background while displaying indicator
        let container: UIView = UIView()
        container.frame = self.view.frame
        container.center = self.view.center
        container.backgroundColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        
        //creating view to display lable and indicator
        let loadingView: UIView = UIView()
        loadingView.frame = CGRect(x: 0, y: 0, width: 118, height: 80)
        loadingView.center = self.view.center
        loadingView.backgroundColor = UIColor.darkGray
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        //Preparing activity indicator to load
        self.activityIndicator = UIActivityIndicatorView()
        self.activityIndicator.frame = CGRect(x: 40, y: 12, width: 40, height: 40)
        self.activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
        loadingView.addSubview(activityIndicator)
        
        //creating label to display message
        let label = UILabel(frame: CGRect(x: 5, y : 55, width :120,height :20))
        label.text = "Uploading..."
        label.textColor = UIColor.white
        label.font = UIFont(name: label.font.fontName, size: 12)
        label.bounds = CGRect(x : 0, y: 0, width : loadingView.frame.size.width / 2, height : loadingView.frame.size.height / 2)
        loadingView.addSubview(label)
        container.addSubview(loadingView)
        self.view.addSubview(container)
        
        self.activityIndicator.startAnimating()
    }
    func stopIndicator()
    {
        UIApplication.shared.endIgnoringInteractionEvents()
        self.activityIndicator.stopAnimating()
        ((self.activityIndicator.superview as UIView?)?.superview as UIView?)!.removeFromSuperview()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        GIDSignIn.sharedInstance()?.clientID = "625280974314-03cteqn6jo7kmghdjb06mcucr13h938g.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        
        //print(idToken)
        if GIDSignIn.sharedInstance().hasAuthInKeychain() {
            // User was previously authenticated to Google. Attempt to sign in.
            GIDSignIn.sharedInstance().signInSilently()
            
            self.startIndicator()
            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                self.stopIndicator()
                self.present(UINavigationController(rootViewController: InitViewController()), animated: true, completion: nil)
            })
            
            
        }
    }
}
extension LoginViewController : GIDSignInUIDelegate,GIDSignInDelegate{
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            // ...
            return
        }
        
        guard let authentication = user.authentication else { return }
        UserInfo.idToken = authentication.idToken
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        
        Auth.auth().signIn(with: credential) { (authResult, error) in
            if let error = error {
                print(error)
                return
            }
            
            print(user)
            print(authResult)
            
            print("User sign in")
        }
        // ...
    }
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        dismiss(animated: true) {() -> Void in }
    }

    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        if let aController = viewController {
            present(aController, animated: true) {() -> Void in }
        }

    }
}
