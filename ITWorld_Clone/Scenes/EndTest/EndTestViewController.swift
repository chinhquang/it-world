//
//  EndTestViewController.swift
//  ITWorld_Clone
//
//  Created by chinh.tq on 8/2/19.
//  Copyright © 2019 chinh.tq. All rights reserved.
//

import UIKit

class EndTestViewController: UIViewController {

    @IBOutlet weak var message: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Finished testing"
        self.navigationItem.setHidesBackButton(true, animated: false)
        message.text = "All of test is finish. You got \(TestViewController.numberOfCorrectAnswer)/\(TestViewController.numberOfQuizzes). Thank you"
        // Do any additional setup after loading the view.
        reset()
    }

    func reset() {
        TestViewController.numberOfCorrectAnswer = 0
        TestViewController.numberOfQuizzes = 0
    }
    @IBAction func returnToMenu(_ sender: UIButton) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
