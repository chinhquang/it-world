//
//  BeginViewController.swift
//  ITWorld_Clone
//
//  Created by chinh.tq on 8/2/19.
//  Copyright © 2019 chinh.tq. All rights reserved.
//

import UIKit

class BeginViewController: UIViewController {

    @IBAction func returnToMenu(_ sender: UIButton) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    @IBAction func beginTheTest(_ sender: UIButton) {
        self.navigationController?.pushViewController(TestViewController(), animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setupColor(color: #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1))
        self.navigationItem.title = "Ready to test"
        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
