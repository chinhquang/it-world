//
//  TestViewController.swift
//  ITWorld_Clone
//
//  Created by chinh.tq on 7/25/19.
//  Copyright © 2019 chinh.tq. All rights reserved.
//

import UIKit

class TestViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    var quizzes = [QuizModel](){
        didSet{
            TestViewController.numberOfQuizzes = quizzes.count
        }
    }
    
    var currentPage : Int = 0
    static var numberOfQuizzes = 0
    static var numberOfCorrectAnswer = 0
    var timer : Timer?
    private func readJson() {
        do {
            if let file = Bundle.main.url(forResource: "test", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    // json is a dictionary
                    print(object)
                } else if let object = json as? [Any] {
                    // json is an array
                    print(object)
                    let array = json as! NSArray
                    for x in array{
                        let item = x as! [String:Any]
                        let quizItem = QuizModel(JSON: item)
                        print(quizItem.options.count)
                        quizzes.append(QuizModel(JSON: item))
                    }
                    quizzes.shuffle()
                } else {
                    print("JSON is invalid")
                }
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.navigationController?.setupColor(color: #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1))
        self.navigationItem.title = "Test"
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.collectionView.register(UINib(nibName: "QuizCell", bundle: nil), forCellWithReuseIdentifier: "QuizCell")
        
        readJson()
        timer = Timer.scheduledTimer(timeInterval: 1,
                                           target: self,
                                           selector: #selector(TestViewController.updateCells),
                                           userInfo: nil, repeats: true)
    }
    @objc func updateCells() {
        let indexPathsArray = collectionView.indexPathsForVisibleItems
        for indexPath in indexPathsArray {
            let cell = collectionView.cellForItem(at: indexPath) as! QuizCell
            if cell.progressCounter == 0{
                print("End counting")
                moveToNextPage()
            }
            cell.progressCounter -= 1
            
        }
    }
    func moveToNextPage() {
        if currentPage < quizzes.count - 1{
            let i = IndexPath(item: currentPage + 1, section: 0)
            currentPage = currentPage + 1
            self.collectionView.scrollToItem(at: i, at: .right, animated: true)
        }else {
            timer?.invalidate()
            print(timer)
            self.navigationController?.pushViewController(EndTestViewController(), animated: true)
        }
    }
}
extension TestViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
    }
}
extension TestViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return quizzes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "QuizCell", for: indexPath) as! QuizCell
        
        cell.quiz = quizzes[indexPath.item]
        cell.setQuizCellLayout(T: quizzes[indexPath.item].question!.questionType!)

        cell.delegate = self
        
        return cell
    }
    
    
}
extension TestViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }
}
extension TestViewController: QuizCellDelegate{
   
    
    func quizCell(_ collectionView: UICollectionView, didTapAt indexPath: IndexPath, _ isCorrect: Bool) {
        
        if isCorrect {
            TestViewController.numberOfCorrectAnswer += 1
        }
        moveToNextPage()
        
    }
    
    
    
}
