//
//  OptionCell.swift
//  ITWorld_Clone
//
//  Created by chinh.tq on 7/25/19.
//  Copyright © 2019 chinh.tq. All rights reserved.
//

import UIKit

class OptionCell: UICollectionViewCell {

    @IBOutlet weak var image: UIImageView!
    
    @IBOutlet weak var imageWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var label: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    var option = OptionModel(){
        didSet{
            image.sd_setImage(with: URL(string: option.img_answer ?? ""), placeholderImage: #imageLiteral(resourceName: "reacticon") )
            label.text = option.label_answer
        }
    }
    func setLayoutCell(T: Type) {
        switch T {
        case .image:
            imageWidthConstraint.constant = self.bounds.width
            
            break
        case .label:
            imageWidthConstraint.constant = 0
            break
        case .both:
            imageWidthConstraint.constant = self.bounds.width / 2
            break
        }
        layoutIfNeeded()
    }
}

