//
//  QuizCell.swift
//  ITWorld_Clone
//
//  Created by chinh.tq on 7/25/19.
//  Copyright © 2019 chinh.tq. All rights reserved.
//

import UIKit
import SDWebImage
protocol QuizCellDelegate: class {
    func quizCell(_ collectionView : UICollectionView, didTapAt indexPath : IndexPath, _ isCorrect: Bool)
   
}
class QuizCell: UICollectionViewCell {
    weak var delegate : QuizCellDelegate?
    static var id_holder = ""
    var quiz = QuizModel(){
        didSet{
            quiz.options.shuffle()
            questionLabel.text = quiz.question?.label_question
            questionImage.sd_setImage(with: URL(string: quiz.question?.img_question ?? ""), placeholderImage: #imageLiteral(resourceName: "reacticon"))
        }
    }
    var progressCounter: Int = 10{
        didSet{
            if self.progressCounter >= 0{
                countdownTimerLabel.text = "Remaining in \(self.progressCounter) seconds"
            }
        }
    }
    @IBOutlet weak var questionImage: UIImageView!
    @IBOutlet weak var countdownTimerLabel: UILabel!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var question: UIView!
    @IBOutlet weak var optionCollectionView: UICollectionView!
    @IBOutlet weak var imageWidthConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.optionCollectionView.delegate = self
        self.optionCollectionView.dataSource = self
        self.optionCollectionView.register(UINib(nibName: "OptionCell", bundle: nil), forCellWithReuseIdentifier: "OptionCell")
        
    }
    
    func setQuizCellLayout(T : Type){
        switch T{
        
        case .image:
        
            imageWidthConstraint.constant = question.frame.width
            questionLabel.text = ""
            break
        case .label:
            imageWidthConstraint.constant = 0
            break
        case .both:
            imageWidthConstraint.constant = optionCollectionView.frame.width / 2
        
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    
    }
}
extension QuizCell: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let isCorrect = quiz.options[indexPath.item].id_option == quiz.question?.id_rightAnswer
        
        delegate?.quizCell(optionCollectionView, didTapAt: indexPath, isCorrect)
    }
}
extension QuizCell: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return quiz.options.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = optionCollectionView.dequeueReusableCell(withReuseIdentifier: "OptionCell", for: indexPath) as! OptionCell
        cell.option = quiz.options[indexPath.item]
        cell.setLayoutCell(T: quiz.options[indexPath.item].optionType!)
        return cell
    }
    
    
}
extension QuizCell: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let horizontalPadding: CGFloat = 10.0
        
        let size  = optionCollectionView.frame
        let cellWidth = (size.width - horizontalPadding * 3) / 2
        
        return CGSize(width: cellWidth, height: cellWidth * 2 / 3)
    }
   

}
