//
//  CategoryViewController.swift
//  ITWorld_Clone
//
//  Created by chinh.tq on 7/23/19.
//  Copyright © 2019 chinh.tq. All rights reserved.
//

import UIKit

class CategoryViewController: UIViewController {
    var categories: [String] = [String]()
    @IBOutlet weak var categoryTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.categoryTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cellID")
        self.categoryTableView.backgroundColor = UIColor.clear
        self.categoryTableView.delegate = self
        self.categoryTableView.dataSource = self
        
        self.categoryTableView.tableFooterView = UIView()
        
        
        categories.append("Calo")
        
        categories.append("Calo")
        
        categories.append("Calo")
        
        categories.append("Calo")
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CategoryViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("dasdsad")
        self.navigationController?.pushViewController(TestViewController(), animated: true)
    }
}
extension CategoryViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = categoryTableView.dequeueReusableCell(withIdentifier: "cellID", for: indexPath)
        cell.textLabel?.text = categories[indexPath.row]
        cell.backgroundColor = #colorLiteral(red: 1, green: 0.6822434635, blue: 0.777230974, alpha: 1)
        return cell
    }
    
}
