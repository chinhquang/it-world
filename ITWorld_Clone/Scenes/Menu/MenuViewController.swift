//
//  MenuViewController.swift
//  ITWorld_Clone
//
//  Created by chinh.tq on 8/1/19.
//  Copyright © 2019 chinh.tq. All rights reserved.
//

import UIKit
import GoogleSignIn
import Firebase
class MenuViewController: UIViewController {

   
    @IBAction func signOut(_ sender: Any) {
        GIDSignIn.sharedInstance().signOut()
        UserInfo.idToken = ""
        navigationController?.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func presentTestViewController(_ sender: UIButton) {
        //self.navigationController?.pushViewController(TestViewController(), animated: true)
        self.present(UINavigationController(rootViewController: BeginViewController()), animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Menu"
        self.setupLanguageButton()
        // Do any additional setup after loading the view.
    }
    func setupLanguageButton() {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "japan"), for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
