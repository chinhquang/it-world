//
//  Downloader.swift
//  ImageList
//
//  Created by chinh.tq on 7/22/19.
//  Copyright © 2019 chinh.tq. All rights reserved.
//

import UIKit
class DownloadImage: Operation {
    
    enum DownloadState {
        case new, success, failed
    }
    
    let fileManager = FileManager.default
    var urlString: String?
    static var url_holder : String?
    var image: UIImage?
    var downloadState: DownloadState = .new
    
    init(urlString: String) {
        self.urlString = urlString
    }
    
    override func main() {
        let url = URL(string: self.urlString!)!
        let filename = url.lastPathComponent
        let imagePAth = (self.getDirectoryPath() as NSString).appendingPathComponent(filename)
        if fileManager.fileExists(atPath: imagePAth){
            
            getImage(filename: filename)
            
            DownloadImage.url_holder = urlString
            print("Fetch : " + filename)
        }
        else {
            
            if self.isCancelled {
                return
            }
            let imageData = NSData(contentsOf: url)
            
            
            
            if (imageData?.length)! > 0 {
                self.image = UIImage(data: imageData! as Data)
                downloadState = .success
                print("download : " + filename)
                
                DownloadImage.url_holder = urlString
                
                cacheImage(filename: filename)
                
                
            } else {
                self.image = UIImage(named: "Close")
                downloadState = .failed
                DownloadImage.url_holder = urlString
                
            }
        }
        print("download_complete \(urlString!)")
        
        
    }
    func cacheImage(filename: String) {
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(filename)
        
        
        if let image = self.image{
            let imageData = image.pngData()
            fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
            
        }
        
    }
    func getImage(filename: String){
        let fileManager = FileManager.default
        let imagePAth = (self.getDirectoryPath() as NSString).appendingPathComponent(filename)
        if fileManager.fileExists(atPath: imagePAth){
            self.image = UIImage(contentsOfFile: imagePAth)
        }else{
            print("No Image")
        }
    }
    func getDirectoryPath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        
        // ./images
        return documentsDirectory
    }
}

class ExcutingOperation {
    static var downloadQueue:OperationQueue = {
        var queue = OperationQueue()
        queue.name = "Download queue"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
}
