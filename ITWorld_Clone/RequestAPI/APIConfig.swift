//
//  APIConfig.swift
//  ImageList
//
//  Created by chinh.tq on 7/18/19.
//  Copyright © 2019 chinh.tq. All rights reserved.
//

import Foundation

public class ApiInfo {
    
    private static var url = "https://pixabay.com/api"
    private static var key = "13058385-bbce3f3836be13c0e625742d7"
    private static let kGET = "GET"
//    method - route
    public static var shareInstance = ApiInfo()
    
    public static var requestApiInfo: [ApiType: [String: Any]] = [
        .getImageList : [
            "method": kGET,
            "route": "/",
        ],
        .other : [
            "method": kGET,
            "route": "/something"
        ]
    ]
    public func getBaseURL ()-> String{
        return ApiInfo.url
    }
    public func getApiKey ()-> String{
        return ApiInfo.key
    }
    
}
