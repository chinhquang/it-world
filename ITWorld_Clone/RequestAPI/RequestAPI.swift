//
//  APIType.swift
//  ImageList
//
//  Created by chinh.tq on 7/18/19.
//  Copyright © 2019 chinh.tq. All rights reserved.
//

import UIKit
import Alamofire



public enum ApiType  {
    case getImageList
    case uploadImage
    case other
}

public class ApiResponse{
    var url: String = ""
    var type: ApiType = .other
    var params: Parameters?
    var response: Any?
    var success = false
    var error: Error?
    
}

typealias Response = (ApiResponse) -> Void

public class API {
    static func requestAPI(apiType : ApiType,param : [String: Any]?, callback : Response?) {
        let routeString: String = ApiInfo.requestApiInfo[apiType]!["route"] as! String
        let url = ApiInfo.shareInstance.getBaseURL() + routeString
        print(url)
        let Url = URL(string: url)
        var request = URLRequest(url: Url!)
        
        request.httpMethod = ApiInfo.requestApiInfo[apiType]!["method"] as? String
        
//        request.allHTTPHeaderFields = []
        let response = ApiResponse()
        response.url = url
        response.params = param
        response.type = apiType
        do {
            let encodedURLRequest = try URLEncoding.queryString.encode(request, with: param)
            Alamofire.request(encodedURLRequest).responseJSON(){
                res in
                
                response.success = true
                response.response = res.result.value
                callback?(response)
                
            }
        } catch {
            response.error = CustomError.urlEncodeError
            callback?(response)
        }
    }
}
