//
//  TimeModel.swift
//  ITWorld_Clone
//
//  Created by chinh.tq on 8/2/19.
//  Copyright © 2019 chinh.tq. All rights reserved.
//

import UIKit

struct TimerModel {
    private var startTime: Date?
    private var offset: TimeInterval = 0
    
    var elapsed : TimeInterval {
        get {
            return self.elapsed(since:Date())
        }
    }
    
    var isRunning = false {
        didSet {
            if isRunning  {
                self.startTime = Date()
            } else {
                if self.startTime != nil{
                    self.offset = self.elapsed
                    self.startTime = nil
                }
            }
        }
    }
    
    func elapsed(since: Date) -> TimeInterval {
        var elapsed = offset
        if let startTime = self.startTime {
            elapsed += -startTime.timeIntervalSince(since)
        }
        return elapsed
    }
}
