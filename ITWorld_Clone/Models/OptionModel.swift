//
//  OptionModel.swift
//  ITWorld_Clone
//
//  Created by chinh.tq on 8/2/19.
//  Copyright © 2019 chinh.tq. All rights reserved.
//

import Foundation
import ObjectMapper
class OptionModel: BaseModel{
    var optionType: Type?
    var id_option: String?
    var img_answer: String?
    var label_answer : String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        id_option <- map["id_option"]
        img_answer <- map["img_answer"]
        label_answer <- map["label_answer"]
        
        if label_answer == nil{
            optionType = .image
        }
        else if  img_answer == nil{
            optionType = .label
        }
        else {
            optionType = .both
        }
    }
}
