//
//  Question.swift
//  ITWorld_Clone
//
//  Created by chinh.tq on 7/24/19.
//  Copyright © 2019 chinh.tq. All rights reserved.
//

import Foundation
import ObjectMapper
enum Type  {
    case image, label, both
}
class BaseModel : Mappable{
    required init?(map: Map){}
    init(){}
    func mapping(map: Map) {}
    
}

class QuizModel {
    var question: QuestionModel?
    var options = [OptionModel]()
    init (JSON : [String:Any]){
        let array = JSON["options"] as! NSArray
        for x in array{
            let json = x as! [String:Any]
            let item = OptionModel(JSON: json)!
            options.append(item)
        }
        question = QuestionModel(JSON: JSON)
    }
    init() {
        
    }
    
}

class QuestionModel: BaseModel{
    var id_question: String?
    var questionType: Type?
    var img_question: String?
    var label_question: String?
    var id_rightAnswer: String?
    override func mapping(map: Map) {
        super.mapping(map: map)
        id_rightAnswer <- map["id_rightAnswer"]
        label_question <- map ["question"]
        img_question <- map["img_question"]
        id_question <- map["id_question"]
        if label_question == nil{
            questionType = .image
        } else if img_question == nil {
            questionType = .label
        } else{
            questionType = .both
        }
    }
    
    
}

