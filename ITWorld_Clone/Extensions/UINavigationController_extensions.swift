//
//  UINavigationController_extensions.swift
//  ITWorld_Clone
//
//  Created by chinh.tq on 7/22/19.
//  Copyright © 2019 chinh.tq. All rights reserved.
//

import UIKit
extension UINavigationController{
    func removeBorder() {
        self.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationBar.shadowImage = UIImage()
    }
    func setupColor(color: UIColor) {
        self.navigationBar.barStyle = UIBarStyle.black
        self.navigationBar.barTintColor = color
        self.navigationBar.tintColor = UIColor.white
    }
    func resetNav() {
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barStyle = .default
        self.navigationController?.navigationBar.backgroundImage(for: .default)
        self.navigationController?.navigationBar.backgroundColor = .none
        self.navigationController?.navigationBar.setBackgroundImage(.none, for: .default)
    }
}
