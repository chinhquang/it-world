//
//  IB_extensions.swift
//  ITWorld_Clone
//
//  Created by chinh.tq on 7/22/19.
//  Copyright © 2019 chinh.tq. All rights reserved.
//

import UIKit
@IBDesignable extension UIButton {
    
    @IBInspectable var shadowColor: UIColor{
        set{
            self.layer.shadowColor = newValue.cgColor
        }
        get{
            guard let color = self.layer.shadowColor else { return UIColor.init()
                
            }
            return UIColor(cgColor: color)
        }
    }
    @IBInspectable var shadowOpacity: Float{
        set{
            self.layer.shadowOpacity = newValue
        }
        get{
            return self.layer.shadowOpacity
        }
    }
    @IBInspectable var shadowOffset: CGSize{
        set{
            self.layer.shadowOffset = newValue
        }
        get{
            return self.layer.shadowOffset
        }
    }
    @IBInspectable var shadowRadius: CGFloat {
        set{
            self.layer.shadowRadius = newValue
        }
        get{
            return self.layer.shadowRadius
        }
    }
    
    //---------------------------------------------------------
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
}

@IBDesignable extension UIImageView {
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
}
@IBDesignable extension UITextField {
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
}

